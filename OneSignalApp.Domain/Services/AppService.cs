﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using OneSignalApp.Domain.Models.AppModels;
using OneSignalApp.Domain.Options;
using OneSignalApp.Domain.Services.Interfaces;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneSignalApp.Domain.Services
{
    public class AppService : IAppService
    {
        private readonly IOptions<OneSignalOptions> _oneSignalAuthOptions;

        public AppService(IOptions<OneSignalOptions> oneSignalAuthOptions)
        {
            _oneSignalAuthOptions = oneSignalAuthOptions;
        }

        public async Task<List<App>> GetApps()
        {
            var client = new RestClient(_oneSignalAuthOptions.Value.APILink);
            var request = new RestRequest("apps", Method.GET);
            request.AddHeader("Authorization", "Basic " + _oneSignalAuthOptions.Value.AuthKey);
            var response = client.Get(request);
            var content = JsonConvert.DeserializeObject<List<App>>(response.Content);
            return content;
        }

        public async Task<App> GetApp(string id)
        {
            var client = new RestClient(_oneSignalAuthOptions.Value.APILink);
            var request = new RestRequest("apps/" + id, Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", "Basic " + _oneSignalAuthOptions.Value.AuthKey);
            var response = client.Get(request);
            var content = JsonConvert.DeserializeObject<App>(response.Content);
            return content;
        }

        public async Task<bool> UpdateApp(string id, UpdateAppModel model)
        {
            var client = new RestClient(_oneSignalAuthOptions.Value.APILink);
            var request = new RestRequest("apps/" + id, Method.PUT);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", "Basic " + _oneSignalAuthOptions.Value.AuthKey);
            request.AddJsonBody(JsonConvert.SerializeObject(model));
            var response = client.Put(request);
            return response.IsSuccessful;
        }

        public async Task<bool> CreateApp(CreateAppModel model)
        {
            var client = new RestClient(_oneSignalAuthOptions.Value.APILink);
            var request = new RestRequest("apps/", Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", "Basic " + _oneSignalAuthOptions.Value.AuthKey);
            request.AddJsonBody(JsonConvert.SerializeObject(model));
            var response = client.Post(request);
            return response.IsSuccessful;
        }
    }
}
