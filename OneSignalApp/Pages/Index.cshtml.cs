﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OneSignalApp.Domain;
using OneSignalApp.Domain.Services.Interfaces;
using System.Threading.Tasks;

namespace OneSignalApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IAccountService _accountService;

        [BindProperty]
        public LoginModel loginModel { get; set; }

        public IndexModel(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public async Task<IActionResult> OnPost()
        {
            var result = await _accountService.Login(loginModel.Email, loginModel.Password);
            if (!result)
            {
                return this.RedirectToPage("/Error");
            }
            return this.RedirectToPage("/AppList");
        }
    }
}
