using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OneSignalApp.Domain.Models.AppModels;
using OneSignalApp.Domain.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneSignalApp.Pages
{
    public class AppListModel : PageModel
    {
        private readonly IAppService _appService;

        [BindProperty]
        public List<App> AppList { get; set; }

        public AppListModel(IAppService appService)
        {
            _appService = appService;
        }

        public async Task<IActionResult> OnGet()
        {
            AppList = await _appService.GetApps();
            return Page();
        }
    }
}
