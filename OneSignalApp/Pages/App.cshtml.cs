using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OneSignalApp.Domain.Models.AppModels;
using OneSignalApp.Domain.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace OneSignalApp.Pages
{
    public class AppModel : PageModel
    {
        private readonly IAppService _appService;

        [BindProperty]
        public App App { get; set; }

        public AppModel(IAppService appService)
        {
            _appService = appService;
        }

        public async Task OnGet(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                App = new App();
                return;
            }
            App = await _appService.GetApp(id);
        }

        public async Task<IActionResult> OnPost()
        {
            if(String.IsNullOrEmpty(App.Id))
            {
                return await CreateApp();
            }
            return await UpdateApp();
        }

        private async Task<IActionResult> UpdateApp()
        {
            UpdateAppModel model = new UpdateAppModel() { Name = App.Name };
            bool result = await _appService.UpdateApp(App.Id, model);
            if (!result)
            {
                throw new System.Exception("Error with updating record " + App.Id);
            }
            return this.RedirectToPage("/AppList");
        }

        private async Task<IActionResult> CreateApp()
        {
            CreateAppModel model = new CreateAppModel() { Name = App.Name };
            bool result = await _appService.CreateApp(model);
            if (!result)
            {
                throw new System.Exception("Error with creating new record!");
            }
            return this.RedirectToPage("/AppList");
        }
    }
}
